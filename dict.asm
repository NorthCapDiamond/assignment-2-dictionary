extern string_equals
global find_word

section .text

find_word: ; pointer (0-term string)-> di; pointer (start of the dict)
	or rsi, 0
	jz .failed_end

	.cycle:
	push rsi
	push rdi
	add rsi, 16

	call string_equals

	pop rdi 
	pop rsi

	or rax, rax
	jnz .found

	mov rsi, qword[rsi]
	or rsi, rsi 
	jz .failed_end
	jmp .cycle



.found:
	mov rax, rsi 
ret

.failed_end:
	xor rax, rax 
ret 

	
